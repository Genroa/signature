import { Ref, computed } from 'vue'
import { Todo } from '../components/models'

export function useDisplayTodo(todos: Ref<Todo[]>) {
  const todoCount = computed(() => todos.value.length)
  return { todoCount }
}
