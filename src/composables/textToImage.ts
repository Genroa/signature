
export const textToImageProps = {
  text: {
    type: String,
    required: true
  },
  title: {
    type: String
  },
  width: {
    type: Number
  },
  height: {
    type: Number
  },
  font: {
    type: String,
    required: true
  },
  fontSize: {
    type: Number,
    required: true
  },
  colour: {
    type: String,
    default: '#010233'
  }
} as const
